var express = require('express')
var fs = require('fs')
var https = require('https')
var app = express()
const path = require('path');
const publicPath = path.join(__dirname, 'build');

app.use(express.static(publicPath));
app.get('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'));
});


https.createServer({
  key: fs.readFileSync('./cert&key/server.key'),
  cert: fs.readFileSync('./cert&key/server.crt')
}, app)
  .listen(3000, function () {
    console.log('Example app listening on port 3000! Go to https://localhost:3000/')
  })