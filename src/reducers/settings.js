let locale = localStorage.getItem('locale');
if (!locale) {
  locale = 'en-US'
}
export default (state = { locale: locale, isOffline: !navigator.onLine }, action) => {
  switch (action.type) {
    case 'CHANGE_LOCALE':
      return {
        ...state,
        locale: action.payload.locale
      };

    case 'IS_OFFLINE':
      return {
        ...state,
        isOffline: action.payload.isOffline
      }

    default:
      return state;
  }
};