export default (state = {}, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        sEmail: action.payload.sEmail,
        token: action.payload.token,
        resStatus: action.payload.resStatus,
        resMessage: action.payload.resMessage,
        dispatchType: action.type
      };

    case 'TOKEN_LOGIN': {
      return {
        token: action.payload.token
      }
    }
    case 'LOGOUT':
      return {
        token: undefined
      }
    default:
      return state;
  }
};