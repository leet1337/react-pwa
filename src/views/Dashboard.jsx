import React, { Component } from "react";
import { connect } from "react-redux";
import Settings from './../components/Settings';
import Navbar from './../components/Navbar'
import './../assets/css/dashboard.css'
import { logout } from './../actions/auth';
import { FormattedMessage } from "react-intl";
import {
  Button
} from 'reactstrap';
import { history } from './../index'

class Dashboard extends Component {
  state = {
    isOpen: false
  }
  render() {
    return (
      <div>
        <Settings />
        <Navbar {...this.props} />
        <div className={'dashboard-button'}>
          <Button onClick={() => { history.push('/qr') }}>
            <FormattedMessage id="Dashboard.QR" />
          </Button>
        </div>
      </div>
    )
  }

}

const mapStateToProps = (state, props) => ({
  locale: state.settings.locale,
  resMessage: state.auth.resMessage,
  resStatus: state.auth.resStatus
})

export default connect(mapStateToProps, { logout }, null, { pure: false })(Dashboard);