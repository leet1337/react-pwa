import React, { Component } from "react";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import NotificationAlert from "react-notification-alert";
import {
  Card, Button, CardHeader, CardFooter, CardBody, CardTitle, FormGroup, Input, Container, Col, Form, FormFeedback, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap';
import { login } from './../actions/auth';
import { changeLocale } from './../actions/settings';
import Settings from './../components/Settings';
import './../assets/css/login.css';

class Login extends Component {
  state = {
    sEmail: "",
    sPassword: "",
    sEmailState: "",
    sPasswordState: "",
  }

  componentWillReceiveProps(props) {
    if (!props.resStatus && props.resMessage) {
      let options = {
        place: "tc",
        message: '  ' + props.resMessage,
        type: "warning",
        icon: "fas fa-exclamation-triangle",
        autoDismiss: 4
      };
      if (this.refs.notificationAlert.state.notifyTC.length < 1) {
        this.refs.notificationAlert.notificationAlert(options);
      }
    }
  }


  verifyEmail = value => {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  };

  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };
  change = (event, stateName, type, stateNameEqualTo, maxValue) => {
    switch (type) {
      case "sEmail":
        if (this.verifyEmail(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "sPassword":
        if (this.verifyLength(event.target.value, 1)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  }


  handleSubmit = (e) => {
    e.preventDefault()
    if (this.state.sEmailState === "has-success" && this.state.sPasswordState === "has-success") {
      this.props.login({
        sEmail: this.state.sEmail,
        sPassword: this.state.sPassword,
        Language: this.props.locale
      })
    } else if (this.state.sEmailState === "" && this.state.sPasswordState === "") {
      this.setState({ sEmailState: "has-danger" });
      this.setState({ sPasswordState: "has-danger" });
    } else if (this.state.sEmailState === "") {
      this.setState({ sEmailState: "has-danger" });
    } else if (this.state.sPasswordState === "") {
      this.setState({ sPasswordState: "has-danger" });
    }
  }

  render() {
    let { sEmailState, sPasswordState } = this.state
    return (
      <div className="content">
        <Settings />
        <div className="rna-container">
          <NotificationAlert ref="notificationAlert" />
        </div>
        <Container>
          <Col className="ml-auto mr-auto" lg="4" md="6">
            <Form className="form" onSubmit={this.handleSubmit}>
              <div className='login-form'>
                <Card className='login-card'>
                  <CardHeader tag="h3">
                    <CardTitle>
                      <FormattedMessage id="Login.LoginText" />
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    <FormGroup className={`has-label ${sEmailState}`}>
                      <label>
                        <FormattedMessage id="Login.EmailText" />
                      </label>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="far fa-envelope" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="sEmail"
                          type="email"
                          autoComplete="off"
                          valid={sEmailState === "has-success"}
                          invalid={sEmailState === "has-danger"}
                          placeholder="test@test.com"
                          onChange={e => this.change(e, "sEmail", "sEmail")}
                        />
                      </InputGroup>
                      <FormFeedback
                        valid={sEmailState === "has-success"}
                        invalid={sEmailState === "has-danger"}
                      >
                        {this.state.sEmailState === "has-danger" ?
                          this.state.sEmail === '' ?
                            (<FormattedMessage id="Login.EmailRequired" />) :
                            (
                              <FormattedMessage id="Login.EmailInvalid" />
                            ) :
                          <span></span>
                        }
                      </FormFeedback>
                    </FormGroup>
                    <FormGroup className={`has-label ${sPasswordState}`}>
                      <label>
                        <FormattedMessage id="Login.PasswordText" />
                      </label>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fas fa-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          id="sPassword"
                          name="sPassword"
                          type="password"
                          autoComplete="off"
                          placeholder="password"
                          valid={sPasswordState === "has-success"}
                          invalid={sPasswordState === "has-danger"}
                          onChange={e =>
                            this.change(e, "sPassword", "sPassword")
                          }
                        />
                      </InputGroup>
                      <FormFeedback
                        valid={sPasswordState === "has-success"}
                        invalid={sPasswordState === "has-danger"}
                      >
                        {this.state.sPasswordState === "has-danger" ? (
                          <FormattedMessage id="Login.PasswordRequired" />
                        ) :
                          <span></span>
                        }
                      </FormFeedback>
                    </FormGroup>
                  </CardBody>
                  <CardFooter>
                    <FormGroup>
                      <Button
                        block
                        color="success"
                        size="lg"
                        disabled={!(sEmailState === "has-success" && sPasswordState === "has-success")}
                      >
                        <FormattedMessage id="Login.LoginText" />
                      </Button>
                    </FormGroup>
                  </CardFooter>
                </Card>
              </div>
            </Form>
          </Col>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return ({
    locale: state.settings.locale,
    resMessage: state.auth.resMessage,
    resStatus: state.auth.resStatus
  })
}

export default connect(mapStateToProps, { login, changeLocale }, null, { pure: false })(Login);