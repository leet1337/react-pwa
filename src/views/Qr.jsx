import React, { Component } from 'react'
import {
  Card
} from 'reactstrap';
import Qr from './../components/Qr';
import Settings from './../components/Settings';
import Navbar from './../components/Navbar'

class QrReader extends Component {
  render() {
    return (
      <div>
        <Settings />
        <Navbar {...this.props} />
        <Card body style={{ alignItems: "center" }}>
          <div style={{ width: '100%', 'maxWidth': '500px' }}>
            <Qr />
          </div>
        </Card>
      </div>
    )
  }
}

export default QrReader