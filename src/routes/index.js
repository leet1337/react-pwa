import React, { lazy } from 'react'
import PublicRoutes from './PublicRoutes'
import PrivateRoutes from './PrivateRoutes'
import { Switch, Redirect } from "react-router-dom";

const Login = lazy(() => import('../views/Login'))
const Dashboard = lazy(() => import('../views/Dashboard'))
const QrReader = lazy(() => import('../views/Qr'))

const Routes = (props) => {
  return (
    <Switch>
      <PublicRoutes
        path="/login"
        component={Login}
        exact={true}
      />
      <PrivateRoutes
        path="/qr"
        component={QrReader}
        exact={true}
      />
      <PrivateRoutes
        path="/dashboard"
        component={Dashboard}
        exact={true}
      />
      <Redirect
        from="/"
        to="/login"
      />
    </Switch>
  )
}

export default Routes;