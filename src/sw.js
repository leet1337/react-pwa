importScripts("workbox-v3.6.3/workbox-sw.js");
workbox.setConfig({ modulePathPrefix: 'workbox-v3.6.3/' })

const precacheManifest = [];
workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(precacheManifest, {});

const dataCacheConfig = {
  cacheName: 'all-data'
}

workbox.routing.registerRoute(/.*/, workbox.strategies.staleWhileRevalidate(dataCacheConfig), 'GET');
workbox.routing.registerRoute(
  /.*.(?:png|jpg|jpeg|css)$/,
  workbox.strategies.staleWhileRevalidate(
    {
      cacheName: 'css'
    }
  )
)