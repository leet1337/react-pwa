export const changeLocale = ({ locale = 'en-US' }) => {
  return (dispatch) => {
    localStorage.setItem('locale', locale)
    dispatch({
      type: 'CHANGE_LOCALE',
      payload: { locale }
    })
  }
}

export const isOffline = ({ isOffline }) => {
  return (dispatch) => {
    dispatch({
      type: "IS_OFFLINE",
      payload: {
        isOffline: isOffline
      }
    })
  }
}