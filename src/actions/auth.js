import { history } from './../index'

export const login = ({ sEmail = '', sPassword = '' }) => {
  return async (dispatch) => {
    if (sEmail === 'test@test.com' && sPassword === 'password') {
      await localStorage.setItem('token', "testToken");
      await dispatch({
        type: 'LOGIN',
        payload: {
          sEmail,
          sPassword,
          token: "testToken",
          userId: 123,
          resStatus: true,
          resMessage: "Login Successfull"
        }
      })
      history.push('/dashboard')
    } else {
      dispatch({
        type: 'LOGIN',
        payload: {
          resStatus: false,
          resMessage: "Email or Password is invalid."
        }
      })
    }
  }
}

export const logout = () => {
  return async (dispatch) => {
    await localStorage.removeItem('token');
    dispatch({
      type: 'LOGOUT'
    })
  }
}