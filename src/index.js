import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk";
import { createBrowserHistory } from "history";
import { Router } from "react-router-dom";
import ReactGA from 'react-ga';
import Intl from "./intl";
import reducers from './reducers'
import Routes from './routes'
import './index.css';
// import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

export const history = createBrowserHistory();

history.listen(location => {
  ReactGA.initialize('UA-134336838-1');
  ReactGA.pageview(location.pathname);
})


const componentEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, {}, componentEnhancers(applyMiddleware(ReduxThunk)))

const jsx = (
  <Provider store={store}>
    <div>
      <Intl>
        <Suspense fallback={<div>Loading...</div>}>
          <Router history={history}>
            <Routes />
          </Router>
        </Suspense>
      </Intl>
    </div>
  </Provider>
)

let hasRendered = false
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('root'));
  }
}

ReactDOM.render(<div>Loading...</div>, document.getElementById('root'));

let token = localStorage.getItem('token');
if (token) {
  if (token === 'testToken') {
    store.dispatch({
      type: 'TOKEN_LOGIN',
      payload: {
        token
      }
    })
    renderApp();
    if (history.location.pathname === '/login') {
      history.push('/dashboard');
    }
  } else {
    renderApp();
    history.push(history.location.pathname)
  }
} else {
  renderApp();
  history.push(history.location.pathname)
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
