import React, { useState } from "react";
import { connect } from "react-redux";
import { FormGroup, Label, Input } from "reactstrap";
import { changeLocale } from './../actions/settings';
import './../assets/css/setting.css'

const Settings = ({ locale, changeLocale }) => {
  const [classes, setclasses] = useState('dropdown');
  return (
    <div className="fixed-plugin">
      <div className={classes}>
        <a
          href="#pablo"
          onClick={e => {
            e.preventDefault();
            if (classes === 'dropdown') {
              setclasses('dropdown show')
            } else {
              setclasses('dropdown')
            }
          }}
        >
          <i className="fa fa-cog fa-2x" />
        </a>
        {
          classes === 'dropdown show'
          &&
          <ul className="dropdown-menu show">
            <li className="header-title">Language</li>
            <li className="adjustments-line">
              <div className="togglebutton switch-sidebar-mini">
                <FormGroup check className="form-check-radio">
                  <Label check>
                    <Input
                      value="en-US"
                      type="radio"
                      onChange={(e) => {
                        changeLocale({
                          locale: e.target.value
                        })
                      }}
                      checked={locale === "en-US" ? true : false}
                    />
                    <span className="form-check-sign" />
                    <span className="header-title" style={{ color: "white" }}>English</span>
                  </Label>
                </FormGroup>
                <FormGroup check className="form-check-radio">
                  <Label check>
                    <Input
                      value="fr-FR"
                      type="radio"
                      onChange={(e) => {
                        changeLocale({
                          locale: e.target.value
                        })
                      }}
                      checked={locale === "fr-FR" ? true : false}
                    />
                    <span className="form-check-sign" />
                    <span className="header-title" style={{ color: "white" }}>French</span>
                  </Label>
                </FormGroup>
              </div>
            </li>
            <li></li>
          </ul>
        }
      </div>
    </div>
  )
}


const mapStateToProps = (state, props) => ({
  locale: state.settings.locale,
})
export default connect(mapStateToProps, { changeLocale })(Settings)