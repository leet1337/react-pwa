import React, { Component } from "react";
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  Button
} from 'reactstrap';
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import { isOffline } from './../actions/settings'
import './../assets/css/dashboard.css'
import { history } from './../index'

class AppNavbar extends Component {
  componentWillMount() {
    window.addEventListener('offline', () => {
      this.props.isOffline({ isOffline: true })
    })
    window.addEventListener('online', () => {
      this.props.isOffline({ isOffline: false })
    })
  }

  render() {
    const { offline } = this.props
    return (
      <div class={offline ? "top-navbar" : ""} >
        <Navbar color="light" light expand="md" className="navbar-custom">
          <NavbarBrand>
            <FormattedMessage id="Dashboard.Title" />
          </NavbarBrand>
          <Nav className="ml-auto" navbar>
            {history.location.pathname !== '/dashboard' && <NavItem>
              <Button onClick={() => { history.push('/dashboard') }} style={{ width: "100%" }}>
                <FormattedMessage id="Dashboard.DashboardText" />
              </Button>
            </NavItem>}
            {history.location.pathname === '/dashboard' && <NavItem>
              <Button onClick={this.props.logout} style={{ width: "100%" }}>
                <FormattedMessage id="Dashboard.Logout" />
              </Button>
            </NavItem>}
          </Nav>
        </Navbar>
      </div >
    )
  }
}

const mapStateToProps = (state, props) => ({
  offline: state.settings.isOffline
})

export default connect(mapStateToProps, { isOffline }, null, { pure: false })(AppNavbar)