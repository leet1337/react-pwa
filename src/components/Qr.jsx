import React, { Component } from 'react'
import QrReader from 'react-qr-reader'

class Qr extends Component {
  state = {
    delay: 300,
    result: 'No result',
  }

  componentDidMount() {
    // if (navigator.getUserMedia) {
    //   navigator.getUserMedia(
    //     {
    //       video: true
    //     },
    //     (localMediaStream) => { this.setState({}) },
    //     (err) => {
    //       alert('The following error occurred when trying to access the camera: ' + err);
    //     }
    //   );
    // } else {
    //   alert('Sorry, browser does not support camera access');
    // }
  }
  handleScan = (data) => {
    if (data) {
      this.setState({
        result: data,
      })
    }
  }
  handleError = (err) => {
    console.error(err)
  }
  render() {
    return (
      <div>
        <QrReader
          delay={this.state.delay}
          onError={this.handleError}
          onScan={this.handleScan}
          style={{ width: '100%' }}
          showViewFinder
        />
        <p>{this.state.result}</p>
      </div>
    )
  }
}

export default Qr;